import * as express from 'express';
import * as mongoose from 'mongoose';
import * as fs from 'fs';
import * as winston from 'winston';
import * as fileUpload from 'express-fileupload';

import * as format from './lib/format';
import { File } from './schema';

import * as upload from './upload';
import * as auth from './auth';

// .env stuff
require('dotenv').config();

const config = JSON.parse(fs.readFileSync('./config/config.json', {encoding: 'utf8'}));

const db = mongoose.connect(`${config.dbconnectionURL}/${config.dbname}`, {
	useNewUrlParser: true, // idfk what any of this does i just copied an example
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true
});

const logger = winston.createLogger({
	level: 'info',
	format: winston.format.combine(
		winston.format.timestamp(),
		winston.format.printf(log => `${format.formatTime(new Date(log.timestamp))} | ${log.message}`)
	),
	transports: [
		new winston.transports.File({filename: `${config.name}-error.log`, level: 'error'}),
		new winston.transports.File({filename: `${config.name}.log`}),
		new winston.transports.Console({
			format: winston.format.combine(
				winston.format.colorize(),
				winston.format.timestamp(),
				winston.format.printf(log => 
					`${format.formatTime(new Date(log.timestamp))} - [${log.level}] ${log.message}`
				)
			),
			level: process.env.DEBUG === 'true' ? 'silly' : 'info'
		})
	]
});

logger.info('connecting to mongodb database');
db.then(() => {
	logger.info('connected to database!');

	const app = express();

	// @ts-ignore
	app.use(express.urlencoded({extended: true}));
	app.use(fileUpload({limits: { fileSize: 50 * 1024 * 1024 }}));
	app.use(express.static('public', {extensions:  ['html', 'htm']}));
	app.use('/assets', express.static('assets'));

	app.set('db', db);
	app.set('config', config);
	app.set('logger', logger);

	upload.run(app);
	auth.run(app);

	app.get('/api/list', async (req, res) => { // only for testing
		const docs = await File.find({});
		// TODO: filter out _id and __v? possibly more
		res.send(docs);
	});

	app.get('*', (req, res) => {
		res.status(404).send('404');
	});

	app.listen(config.port, () => {
		logger.info(`expressjs server launched on port ${config.port}, should now function properly`);
	});
});