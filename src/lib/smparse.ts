export function parseSM(data: string) {
	data = data.replace(/[\n\r]/g,'');

	// steps
	const difficulties = [];
	const steps = data.split('#NOTES:');
	steps.slice(1);

	for (const step of steps) {
		if (step.includes(';')) {
			const diff: any = {};
			const stepsSplit = step.split(';')[0].split(':').map(seg => seg.trim());

			if (stepsSplit.length === 6) {
				diff.type = stepsSplit[0];
				diff.name = stepsSplit[1];
				diff.difficulty = stepsSplit[2];
				diff.rating = Number(stepsSplit[3]);
				diff.radarvalues = stepsSplit[4].split(',').map(v => Number(v));

				difficulties.push(diff);
			}
		}
	}

	// metadata
	const lines = data.split(';').filter(l => l.startsWith('#'));
	const obj: any = {};

	for (const l of lines) {
		const key = l.split(':')[0].slice(1);
		let value: any = l.split(':')[1];

		if (value !== '' && !(key === 'FGCHANGES' || key === 'BGCHANGES' || key === 'BETTERBGCHANGES' || key === 'SPELLCARDS')) { // no
			if (!isNaN(value)) value = Number(value);
			if (value === 'YES') value = true;
			if (value === 'NO') value = false;
			if (typeof value === 'string' && value.includes('=')) { // likely a map
				const keys = value.split(',').map(v => v.split('=')[0]);
				const values = value.split(',').map(v => v.split('=')[1]);
				const map = {};

				for (const i in keys) {
					map[Number(keys[i])] = Number(values[i]); // afaik maps are only numbers?
				}

				value = map;
			}
	
			obj[key.toLowerCase()] = value;
		}
	}

	obj.charts = difficulties;

	return obj;
}