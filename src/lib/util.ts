import * as fs from 'fs';

export function returnStatic(page) {
	return (req, res) => {
		fs.readFile(`src/html/${page}`, 'utf8', (err, data) => {
			if (err) throw err;
			res.send(data);
		});
	};
}