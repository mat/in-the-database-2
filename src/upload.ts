import { tmpdir } from 'os';
import * as fs from 'fs';
const StreamZip = require('node-stream-zip');

import { returnStatic } from './lib/util';
import { parseSM } from './lib/smparse';
import { File } from './schema';

export function run(app) {
	const logger = app.get('logger');
	
	app.post('/api/upload', async (req, res) => { // only for testing, very abusable
		if (!req.files) return res.status(400).send('No files were given');
		const file = req.files.file;
	
		if (file.mimetype !== 'application/zip' && file.mimetype !== 'application/x-zip-compressed') return res.status(400).send('Invalid filetype');

		const dir = tmpdir() + '/' + file.md5;
		fs.writeFile(dir, file.data, (err) => {
			if (err) throw err;

			const zip = new StreamZip({
				file: dir,
				storeEntries: true
			});

			zip.on('ready', () => {
				const smFile = Object.values(zip.entries()).find((f: any) =>
					!f.isDirectory && (f.name.endsWith('.sm'))
				);

				if (!smFile) {
					res.status(400).send('No .sm found');
				} else {
					const data = zip.entryDataSync((smFile as any).name);
					const chart = parseSM(data.toString());

					logger.info(`${chart.artist} - ${chart.title} was just uploaded`);

					const file = new File(chart);
					file.save();

					// TODO: filter out _id and __v? possibly more
					res.send(chart);
				}

				zip.close();
				fs.unlink(dir, (err) => {
					if (err) throw err;
				});
			});
		});
	});
}