import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;
 
const Sample = new Schema({
	start: {type: Number, default: 0},
	length: {type: Number, default: 0}
});

const Chart = new Schema({
	type: {type: String, default: 'dance-single'},
	name: {type: String, default: ''},
	difficulty: {type: String, default: 'Challenge'},
	rating: {type: Number, default: 0},
	radarvalues: [Number]
});

const FileSchema = new Schema({
	title: {type: String, default: 'unknown'},
	titleTranslit: String,
	artist: {type: String, default: 'unknown'},
	artistTranslit: String,
	subtitle: String,
	subtitleTranslit: String,
	credit: String,
	uploader: {type: String, default: '00000000-0000-4000-a000-000000000000'},
	sample: Sample,
	bpms: {type: Object, default: {'0': 0}},
	charts: [Chart]
});

export const File = mongoose.model('File', FileSchema);

const UserSchema = new Schema({ // this is pretty much just a discord user lol
	id: String, // cus longass number
	approved: Boolean
});

export const User = mongoose.model('User', UserSchema);